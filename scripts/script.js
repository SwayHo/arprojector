/**
 * (c) Facebook, Inc. and its affiliates. Confidential and proprietary.
 */

//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//
// For projects created with v87 onwards, JavaScript is always executed in strict mode.
//==============================================================================

// How to load in modules
const T = require('Textures');
const R = require('Reactive');
const Shaders = require('Shaders');
const Patches = require('Patches');

// Use export keyword to make a symbol available in scripting debug console
export const Diagnostics = require('Diagnostics');


async function findObj(){
    const galleryTex = await T.findFirst('galleryTexture0');

    return {
        galleryTexture : galleryTex
    }
}

findObj().then(obj => {
    const ratio = R.div(obj.galleryTexture.height, obj.galleryTexture.width);
    Patches.inputs.setScalar('ratio', ratio);
    ratio.monitor().subscribe(function (event){
        // Patches.inputs.setScalar('ratio', ratio);

    });
})
